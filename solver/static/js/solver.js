onload = function init() {
  document.getElementById("submitButton").onclick = function () {
    document.getElementById("place").innerHTML = "";
    $("#place").append(`<div class="chessboard" id="chessboard">`);

    var jumlahRook = document.getElementById("jumlahRook").value;
    var jumlahRookInt = parseInt(jumlahRook);

    document.getElementById("chessboard").style.width = jumlahRookInt * 80;
    document.getElementById("chessboard").style.height = jumlahRookInt * 80;

    solve(jumlahRook);
  };
};

function solve(jumlahRook) {
  $.ajax({
    url: "getmodel/",
    type: "POST",
    dataType: "json",
    data: {
      jumlahrook: jumlahRook,
    },
    success: function (response) {
      $.each(response["data"], function (i, item) {
        if (Math.floor(i / jumlahRook) % 2 == 0 || jumlahRook % 2 == 1) {
          if (i % 2 == 0) {
            addBlackTile(item);
          } else {
            addWhiteTile(item);
          }
        } else {
          if (i % 2 == 0) {
            addWhiteTile(item);
          } else {
            addBlackTile(item);
          }
        }
      });
    },
  });
}

function addBlackTile(item) {
  if (item == 1) {
    $("#chessboard").append(
      `
      <div class="black">&#9820;</div>
      `
    );
  } else {
    $("#chessboard").append(
      `
      <div class="black"></div>
      `
    );
  }
}

function addWhiteTile(item) {
  if (item == 1) {
    $("#chessboard").append(
      `
      <div class="white">&#9820;</div>
      `
    );
  } else {
    $("#chessboard").append(
      `
      <div class="white"></div>
      `
    );
  }
}
