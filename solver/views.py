from django.shortcuts import render

from django.http import JsonResponse

from .minisatSolver import minisolvers
import random

from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
    return render(request, 'board.html')

@csrf_exempt
def getModel(request):
    S = minisolvers.MinisatSolver()
    n = int(request.POST.get('jumlahrook'))
    allPion = []
    table = []
    count = 0

    for i in range (n*n):
        allPion.append(str(i+1))

    for i in range(n):
        table.append([])
        for j in range (n):
            table[i].append(count+1)
            S.new_var()
            count += 1

    dnf = []

    #Nambahin Row
    for row in table:
        for l in row:
            dnf.append(l)
        S.add_clause(dnf)
        dnf = []
        for i in range (n-1):
            for j in range (i+1,n):
                dnf.append(-row[i])
                dnf.append(-row[j])
                S.add_clause(dnf)
                dnf = []

    #Nambahin Column
    for i in range (n):
        for j in range (n):
            dnf.append(table[j][i])
        S.add_clause(dnf)
        dnf = []
        for k in range (n-1):
            for l in range (k+1,n):
                dnf.append(-table[k][i])
                dnf.append(-table[l][i])
                S.add_clause(dnf)
                dnf = []

    times = random.randint(1,n)
    for i in range(times):
        S.solve()
        S.block_model()
        S.solve()

    # print(list(S.get_model_trues()))

    data = list(S.get_model())
    rookModel = {
        'data' : data
    }

    return JsonResponse(rookModel)
 